package com.example.mytest02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mytest02Application {

	public static void main(String[] args) {
		SpringApplication.run(Mytest02Application.class, args);
	}

}
